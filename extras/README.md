Pour découper un pays trop grand

Il faut découper le fichier polygone du pays en plusieurs polygone, un script non optimisé existe pour essayer de découper les polygones en aires égales et du coup avoir des fichier peut etre de la même taille.

```python
python split_poly.py FILE.poly COUNT_MIN
```

Cela va vous créer le fichier ``FILE_1.poly``,``FILE_2.poly``...

Pour découper un fichier pbf
```
osmosis --read-pbf file="FILE.osm.pbf" --bounding-polygon file="FILE.poly" completeWays=yes  --write-pbf file="NEW_NAME.osm.pbf"
```